<?php

class Usuario extends ORM{

	protected static $table = "usuarios";
	public $id,$username,$password,$email,$fecha_nacimiento,$nombres,$apellidos;
        public $has_many = array(
            'friends'=>array(
                'class'=>'Usuario',
                'join_self_as'=>'id_user',
                'join_other_as'=>'id_friend',
                'join_table'=>'friends_x_users'
            ),
            'posts'=>array(
                'class'=>'Publicacion',
                'join_self_as'=>'id_usuario',
                'join_other_as'=>'id',
                'join_table'=>'publicaciones'
            )
        );
                
	function __construct($id,$username,$password,$email,$fecha_nacimiento = null,$nombres = null,$apellidos = null){

		$this->id = $id;
		$this->username = $username;
		$this->password = $password;
		$this->email = $email;
		$this->fecha_nacimiento = $fecha_nacimiento;
		$this->nombres = $nombres;
		$this->apellidos = $apellidos;

	}

	function get( $id ){
		$data = array_shift(self::where("id",$id));
		$usr = new self($data["id"],$data["username"],$data["password"],$data["email"],$data["fecha_nacimiento"],$data["nombres"],$data["apellidos"]);
		return $usr;
	}

	function getByUsername( $username ){
		$data = array_shift(self::where("username",$username));
		Logger::debug("data",$data,"getByUsername");
		$usr = new self($data["id"],$data["username"],$data["password"],$data["email"],$data["fecha_nacimiento"],$data["nombres"],$data["apellidos"]);
		return $usr;
	}

}