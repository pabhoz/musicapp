function displayAdvancedForm(trigger){
	$(trigger).next("div").slideToggle();
}

function evaluateChars(jqObject){
	var chars = jqObject.val().length;
	var counter = jqObject.next(".charCounter");
	var max = counter.data("max");

	if( chars <= max){
		counter.text(chars+"/"+max);
		if(counter.hasClass("warning")){counter.removeClass("warning");}
	}else{
		jqObject.val(jqObject.val().substring(0,max));
		counter.addClass("warning");
	}
}