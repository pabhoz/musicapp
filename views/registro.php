<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width"><!--, user-scalable=no">-->

		<title>Registro || Procesamiento de Formularios</title>
		<meta name="description" content="Ejemplo de procesamiento de formularios">
		<meta name="author" content="Pabhoz">

		<link rel="stylesheet" href="./css/main.css">

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="./js/libs/jquery-1.11.0.min.js"></script>
		<script src="./js/src.js"></script>

	</head>
	<body>
		
		<form  method="POST" enctype="multipart/form-data" autocomplete="off">
			<div class="title">Registro de Usuario</div>
			<input type="text" name="username" placeholder="Username" required>
			<input type="password" name="password" placeholder="Password" required>
			<input type="email" name="email" placeholder="e-Mail" required>
			<input type="file" name="avatar" >
			<div class="button formSlider" onclick="displayAdvancedForm(this)">Mostrar campos opcionales</div>
			<div class="hidden">
                                <input type="text" name="fecha_nacimiento" placeholder="fecha de nacimiento (1991-11-14)">
                                <input type="text" name="nombres" placeholder="nombres">
                                <input type="text" name="apellidos" placeholder="apellidos">
                                                                
				<!--textarea class="toCount" name="description" placeholder="Cómo te describirías?"></textarea>
				<div class="charCounter" data-max="150">0/0</div>
				<input type="text" name="favGenre" placeholder="Genero musical favorito">
				<input type="text" name="favGroup" placeholder="Grupo Favorito (PTV,ETF,SWS)">
				<input type="text" name="favSong" placeholder="Canción favorita">
				<input type="file" name="songFile"-->
			</div>
			<button>Registrar</button>
			<iframe></iframe>
		</form>

		<script>

			$(function(){
                        
                        //Listeners
                        $(".charCounter").each(function(){
                                 var counter = $(this);
                                 counter.text("0/"+counter.data("max"));
                        });

                         $(".toCount").keypress(function(){ evaluateChars( $(this) ); })
                                      .keyup(function(){ evaluateChars( $(this) ); });
                        //Formulario
                        $('form').submit(function(e){

                        	var formData = {
                        		'username': $('input[name=username]').val(),
					            'password': $('input[name=password]').val(),
					            'email': $('input[name=email]').val(),
					            'fecha_nacimiento': $('input[name=fecha_nacimiento]').val(),
					            'nombres': $('input[name=nombres]').val(),
					            'apellidos': $('input[name=apellidos]').val()
                        	};

                        	console.log(formData);

                        	$.ajax({

                        		type: 'POST',
                        		url: '<?php echo URL."registrar"; ?>',
                        		data: formData//,
                        		//dataType: 'json',
                        		//encode: true

                        	}).done(function(data){
                        		var respuesta = JSON.parse(data);
                        		if(respuesta.error == 0){
                        			alert(respuesta.message);
                        		}else{
                        			alert("Problemas en el registro, intente mas tarde");
                        		}
                        	});

                        	e.preventDefault();

                        });

            });
		</script>
	</body>
</html>