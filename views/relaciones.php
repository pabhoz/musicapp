<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width"><!--, user-scalable=no">-->

		<title>Relaciones || Procesamiento de Formularios</title>
		<meta name="description" content="Ejemplo de procesamiento de formularios">
		<meta name="author" content="Pabhoz">

		<link rel="stylesheet" href="./css/main.css">

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="./js/libs/jquery-1.11.0.min.js"></script>
	</head>
	<body>
		<form>
			<?php
				$users = Usuario::getAll();
				/*foreach ($users as $key => $usuario) {
					$usuarios[] = new Usuario($usuario["id"],$usuario["username"],$usuario["password"],$usuario["email"]);
				}
				print_r($usuarios);*/
			?>
			<select id="id_user">
				<option>Seleccione un usuario</option>
				<?php
					foreach ($users as $key => $usuario) {
						echo '<option value="'.$usuario["id"].'">'.$usuario["username"].'</option>';
					}
				?>
			</select>
			<select id="id_friend">
				<option>Seleccione un usuario</option>
				<?php
					foreach ($users as $key => $usuario) {
						echo '<option value="'.$usuario["id"].'">'.$usuario["username"].'</option>';
					}
				?>
			</select>
			<button>Relacionar</button>
		</form>
		<script>

			$(function(){
                        
                        //Formulario
                        $('form').submit(function(e){

                        	var formData = {
                        		'id_user': $('#id_user').val(),
					            'id_friend': $('#id_friend').val()
                        	};

                        	console.log(formData);

                        	$.ajax({

                        		type: 'POST',
                        		url: '<?php echo URL."relacionar"; ?>',
                        		data: formData//,
                        		//dataType: 'json',
                        		//encode: true

                        	}).done(function(data){
                        		alert("listo");
                        	});

                        	e.preventDefault();

                        });

            });
		</script>
	</body>
	</html>