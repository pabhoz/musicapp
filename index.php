<?php
	
	require "config.php";


	/**
		cargarClasesAutonomamente
	*
	*	Carga las clases al momento de ser necesitadas
	*	Ej: Carga la clase Usuario al crear una instancia del mismo
	*	$usr = new Usuario();
	*
	*/
	function cargarClasesAutonomamente($class){

		if(file_exists(LIBS.$class.".php")){
			require LIBS.$class.".php";
		}

	}

	//Registramos la función a la cola de autocarga
	spl_autoload_register('cargarClasesAutonomamente');

	/**
		Ejemplo
	*	musicapp.com/avicii/levels
	*
	*	1. Es una carpeta lo que él busca? existe? -> no
	*	2. Es un archivo lo que él busca? existe? -> no
	*	3. es una librería lo que él busca? existe? -> no
	*
	*	Entonces?
	*	Mmmm vea, siga derecho y pregunte en esa puerta del apartamento index si allí
	*	está la persona que usted busca.
	*
	*/
	$url = (isset($_GET["url"])) ? $_GET["url"] : "index";
	$url = explode("/", $url); // [0] => avicii, [1] => levels

	if( file_exists( "./views/".$url[0].".php" )){
		require "./views/".$url[0].".php";
	}elseif( file_exists( "./controllers/".$url[0].".php") ){
		require "./controllers/".$url[0].".php";
	}else{
		require './views/404.php';
	}