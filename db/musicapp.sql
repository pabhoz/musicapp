SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema musicapp
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `musicapp` ;
CREATE SCHEMA IF NOT EXISTS `musicapp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `musicapp` ;

-- -----------------------------------------------------
-- Table `musicapp`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`usuarios` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`usuarios` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `fecha_nacimiento` DATE NULL,
  `nombres` VARCHAR(45) NULL,
  `apellidos` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE UNIQUE INDEX `username_UNIQUE` ON `musicapp`.`usuarios` (`username` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`friends_x_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`friends_x_users` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`friends_x_users` (
  `id_user` INT NOT NULL,
  `id_friend` INT NOT NULL,
  PRIMARY KEY (`id_user`, `id_friend`),
  CONSTRAINT `user_f_x_u_fk`
    FOREIGN KEY (`id_user`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `friend_f_x_u_fk`
    FOREIGN KEY (`id_friend`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `friend_f_x_u_fk_idx` ON `musicapp`.`friends_x_users` (`id_friend` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`artistas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`artistas` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`artistas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `ano_inicio` YEAR NOT NULL,
  `ano_fin` YEAR NULL,
  `popularidad` FLOAT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`generos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`generos` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`generos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `genero` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`canciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`canciones` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`canciones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `genero` INT NOT NULL,
  `ano` DATE NOT NULL,
  `popularidad` FLOAT NULL,
  `artista` INT NOT NULL,
  `explicit` TINYINT(1) NOT NULL DEFAULT 0,
  `uri` VARCHAR(100) NULL DEFAULT 0,
  `duracion` INT NOT NULL,
  `formato` VARCHAR(5) NOT NULL,
  `peso_KB` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `canciones_artistas_fk`
    FOREIGN KEY (`artista`)
    REFERENCES `musicapp`.`artistas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `canciones_genero_fk`
    FOREIGN KEY (`genero`)
    REFERENCES `musicapp`.`generos` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `canciones_artistas_fk_idx` ON `musicapp`.`canciones` (`artista` ASC);

SHOW WARNINGS;
CREATE INDEX `canciones_genero_fk_idx` ON `musicapp`.`canciones` (`genero` ASC);

SHOW WARNINGS;
CREATE UNIQUE INDEX `uri_UNIQUE` ON `musicapp`.`canciones` (`uri` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`roles` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE UNIQUE INDEX `rol_UNIQUE` ON `musicapp`.`roles` (`rol` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`credenciales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`credenciales` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`credenciales` (
  `idUsuario` INT NOT NULL,
  `idRol` INT NOT NULL,
  PRIMARY KEY (`idUsuario`, `idRol`),
  CONSTRAINT `credenciales_usuarios_fk`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `credenciales_roles_fk`
    FOREIGN KEY (`idRol`)
    REFERENCES `musicapp`.`roles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `credenciales_roles_fk_idx` ON `musicapp`.`credenciales` (`idRol` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`tipo_album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`tipo_album` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`tipo_album` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo_album` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE UNIQUE INDEX `tipo_álbum_UNIQUE` ON `musicapp`.`tipo_album` (`tipo_album` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`album` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`album` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `ano` YEAR NOT NULL,
  `tipo_album` INT NOT NULL,
  `caratula` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `álbumes_tipo_álbum_fk`
    FOREIGN KEY (`tipo_album`)
    REFERENCES `musicapp`.`tipo_album` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `álbumes_tipo_álbum_fk_idx` ON `musicapp`.`album` (`tipo_album` ASC);

SHOW WARNINGS;
CREATE UNIQUE INDEX `portada_UNIQUE` ON `musicapp`.`album` (`caratula` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`canciones_x_albumes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`canciones_x_albumes` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`canciones_x_albumes` (
  `id_cancion` INT NOT NULL,
  `id_album` INT NOT NULL,
  `n_pista` INT NOT NULL,
  PRIMARY KEY (`id_cancion`, `id_album`),
  CONSTRAINT `c_x_a_cancion_fk`
    FOREIGN KEY (`id_cancion`)
    REFERENCES `musicapp`.`canciones` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `c_x_a_album_fk`
    FOREIGN KEY (`id_album`)
    REFERENCES `musicapp`.`album` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `c_x_a_album_fk_idx` ON `musicapp`.`canciones_x_albumes` (`id_album` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`usuarios_x_artistas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`usuarios_x_artistas` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`usuarios_x_artistas` (
  `id_usuario` INT NOT NULL,
  `id_artista` INT NOT NULL,
  PRIMARY KEY (`id_usuario`, `id_artista`),
  CONSTRAINT `usuarios_u_x_a_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `artistas_u_x_a_fk`
    FOREIGN KEY (`id_artista`)
    REFERENCES `musicapp`.`artistas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `artistas_u_x_a_fk_idx` ON `musicapp`.`usuarios_x_artistas` (`id_artista` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`subscripciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`subscripciones` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`subscripciones` (
  `id_usuario` INT NOT NULL,
  `id_artista` INT NOT NULL,
  `fecha` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id_usuario`, `id_artista`),
  CONSTRAINT `usuarios_subscripciones_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `artistas_subscripciones_fk`
    FOREIGN KEY (`id_artista`)
    REFERENCES `musicapp`.`artistas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `artistas_subscripciones_fk_idx` ON `musicapp`.`subscripciones` (`id_artista` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`niveles_de_privacidad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`niveles_de_privacidad` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`niveles_de_privacidad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nivel` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE UNIQUE INDEX `nivel_UNIQUE` ON `musicapp`.`niveles_de_privacidad` (`nivel` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`publicaciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`publicaciones` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`publicaciones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `fecha` TIMESTAMP NOT NULL,
  `contenido` TEXT NOT NULL,
  `privacidad` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  CONSTRAINT `usuarios_publicaciones_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `privacidad_publicaciones_fk`
    FOREIGN KEY (`privacidad`)
    REFERENCES `musicapp`.`niveles_de_privacidad` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `usuarios_publicaciones_fk_idx` ON `musicapp`.`publicaciones` (`id_usuario` ASC);

SHOW WARNINGS;
CREATE INDEX `privacidad_publicaciones_fk_idx` ON `musicapp`.`publicaciones` (`privacidad` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`imagenes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`imagenes` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`imagenes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `uri` VARCHAR(45) NOT NULL,
  `formato` VARCHAR(5) NOT NULL,
  `peso_KB` INT NOT NULL,
  `privacidad` INT NOT NULL DEFAULT 1,
  `fecha_adicion` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `privacidad_imagenes_fk`
    FOREIGN KEY (`privacidad`)
    REFERENCES `musicapp`.`niveles_de_privacidad` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `usuario_imagenes_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `privacidad_imagenes_fk_idx` ON `musicapp`.`imagenes` (`privacidad` ASC);

SHOW WARNINGS;
CREATE INDEX `usuario_imagenes_fk_idx` ON `musicapp`.`imagenes` (`id_usuario` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`avatares`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`avatares` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`avatares` (
  `id_usuario` INT NOT NULL,
  `id_imagen` INT NOT NULL,
  `usando` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_usuario`, `id_imagen`),
  CONSTRAINT `usuario_avatares_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `imagen_avatares_fk`
    FOREIGN KEY (`id_imagen`)
    REFERENCES `musicapp`.`imagenes` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `imagen_avatares_fk_idx` ON `musicapp`.`avatares` (`id_imagen` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`portadas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`portadas` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`portadas` (
  `id_usuario` INT NOT NULL,
  `id_imagen` INT NOT NULL,
  `usando` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_usuario`, `id_imagen`),
  CONSTRAINT `usuario_portadas_fk`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `imagen_portadas_fk`
    FOREIGN KEY (`id_imagen`)
    REFERENCES `musicapp`.`imagenes` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `imagen_portadas_fk_idx` ON `musicapp`.`portadas` (`id_imagen` ASC);

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `musicapp`.`publicaciones_artistas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `musicapp`.`publicaciones_artistas` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `musicapp`.`publicaciones_artistas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_artista` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `fecha` TIMESTAMP NOT NULL,
  `contenido` TEXT NOT NULL,
  `privacidad` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  CONSTRAINT `artista_publicaciones artista_fk0`
    FOREIGN KEY (`id_artista`)
    REFERENCES `musicapp`.`artistas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `usuario_publicaciones_artista_fk0`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `musicapp`.`usuarios` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `privacidad_publicaciones_artistas_fk0`
    FOREIGN KEY (`privacidad`)
    REFERENCES `musicapp`.`niveles_de_privacidad` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `artista_publicaciones artista_fk_idx` ON `musicapp`.`publicaciones_artistas` (`id_artista` ASC);

SHOW WARNINGS;
CREATE INDEX `usuario_publicaciones_artista_fk_idx` ON `musicapp`.`publicaciones_artistas` (`id_usuario` ASC);

SHOW WARNINGS;
CREATE INDEX `privacidad_publicaciones_fk_idx` ON `musicapp`.`publicaciones_artistas` (`privacidad` ASC);

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
